package com.bonify.question.one.questionOne;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class QuestionOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionOneApplication.class, args);
	}
        
        @Bean
        public CommandLineRunner processContract() {
            return (args) -> {
                if (args.length != 1)
                    System.out.println("Invalid type provided");
                else 
                    System.out.println("Processed " + args[0]);
            };
        }
}
